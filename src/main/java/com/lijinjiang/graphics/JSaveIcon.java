package com.lijinjiang.graphics;

import javax.swing.*;
import java.awt.*;

/**
 * @Description 保存图标
 * @Author lijinjiang
 * @Date 2024-12-30 10:52
 */
public class JSaveIcon implements Icon {
    private int width;
    private int height;

    public JSaveIcon(int width, int height) {
        setDimension(width, height);
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2d = (Graphics2D) g;
        //设置抗锯齿和背景颜色
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //设置粗细
        g2d.setStroke(new BasicStroke(2F));
        //设置颜色
        g2d.setColor(Color.BLACK);
        //绘制图形
        g2d.drawLine(9 * width / 32, 15 * height / 32, 16 * width / 32, 22 * height / 32);
        g2d.drawLine(16 * width / 32, 22 * height / 32, 23 * width / 32, 15 * height / 32);
        g2d.drawLine(16 * width / 32, 7 * height / 32, 16 * width / 32, 22 * height / 32);
        g2d.drawLine(8 * width / 32, 25 * height / 32, 24 * width / 32, 25 * height / 32);
    }

    @Override
    public int getIconWidth() {
        return width;
    }

    @Override
    public int getIconHeight() {
        return height;
    }
}
