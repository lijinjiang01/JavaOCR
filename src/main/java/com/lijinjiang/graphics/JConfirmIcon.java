package com.lijinjiang.graphics;

import javax.swing.*;
import java.awt.*;

/**
 * @Description 确认图标
 * @Author lijinjiang
 * @Date 2024-12-30 10:52
 */
public class JConfirmIcon implements Icon {
    private int width;
    private int height;

    public JConfirmIcon(int width, int height) {
        setDimension(width, height);
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2d = (Graphics2D) g;
        //设置抗锯齿和背景颜色
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //设置粗细
        g2d.setStroke(new BasicStroke(2.2F));
        //设置颜色
        g2d.setColor(new Color(38, 126, 240));
        //绘制图形
        g2d.drawLine(7 * width / 32, 16 * height / 32, 13 * width / 32, 22 * height / 32);
        g2d.drawLine(13 * width / 32, 22 * height / 32, 25 * width / 32, 10 * height / 32);
    }

    @Override
    public int getIconWidth() {
        return width;
    }

    @Override
    public int getIconHeight() {
        return height;
    }
}
