package com.lijinjiang;

import com.lijinjiang.beautyeye.BeautyEyeLNFHelper;
import com.lijinjiang.view.MainFrame;

import javax.swing.*;

public class App {
    public static void main(String[] args) {
        try {
            /*
             * 设置本属性将改变窗口边框样式定义
             * 系统默认样式 : osLookAndFeelDecorated
             * 强立体半透明 : translucencyAppleLike
             * 弱立体半透明 : translucencySmallShadow
             * 普通不透明 : generalNoTranslucencyShadow
             */
            BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;
            BeautyEyeLNFHelper.launchBeautyEyeLNF();
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            new MainFrame();//初始化主窗口
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
